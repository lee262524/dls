import QtQuick 2.9
import de.igh.dls 1.0
import QtQuick.Controls 2.5

Menu { //keep in sync with Menu from Graph.cpp
    title: qsTr("&Menu")
    Action { 
        text: qsTr("Messlinie &fixieren")
        onTriggered:dlsWidget.actionTrigger("fixMeasuringAction")
    }

    Action { 
        text: qsTr("Messlinie &entfernen") 
        onTriggered:dlsWidget.actionTrigger("removeMeasuringAction")
    }
    MenuSeparator { }
    Action { 
        text: qsTr("&Vorherige Ansicht") 
        onTriggered:dlsWidget.actionTrigger("prevViewAction")
    }
    Action { 
        text: qsTr("&Nächste Ansicht") 
        onTriggered:dlsWidget.actionTrigger("nextViewAction")
    }
    MenuSeparator { }
    Action { 
        text: qsTr("Akt&ualisieren") 
        onTriggered:dlsWidget.actionTrigger("loadDataAction")
    }
    MenuSeparator { }
    Action { 
        text: qsTr("&Zoom") 
        onTriggered:dlsWidget.actionTrigger("zoomAction")
    }
    Action { 
        text: qsTr("&Verschieben") 
        onTriggered:dlsWidget.actionTrigger("panAction")
    }
    Action { 
        text: qsTr("&Messen") 
        onTriggered:dlsWidget.actionTrigger("measureAction")
    }

    Action { 
        text: qsTr("Hereinzoomen") 
        onTriggered:dlsWidget.actionTrigger("zoomInAction")
    }
    Action { 
        text: qsTr("Herauszoomen") 
        onTriggered:dlsWidget.actionTrigger("zoomOutAction")
    }
    Action { 
        text: qsTr("Gesamten Zeitbereich anzeigen") 
        onTriggered:dlsWidget.actionTrigger("zoomResetAction")
    }
    Menu {
        title:"Gehe zu Datum"
        Action { 
            text: qsTr("Heute") 
            onTriggered:dlsWidget.actionTrigger("gotoTodayAction")
        }
        Action { 
            text: qsTr("Gestern") 
            onTriggered:dlsWidget.actionTrigger("gotoYesterdayAction")
        }
        Action { 
            text: qsTr("Diese Woche") 
            onTriggered:dlsWidget.actionTrigger("gotoThisWeekAction")
        }
        Action { 
            text: qsTr("Letzte Woche") 
            onTriggered:dlsWidget.actionTrigger("gotoLastWeekAction")
        }
        Action { 
            text: qsTr("Dieser Monat") 
            onTriggered:dlsWidget.actionTrigger("gotoThisMonthAction")
        }
        Action { 
            text: qsTr("Letzter Monat") 
            onTriggered:dlsWidget.actionTrigger("gotoLastMonthAction")
        }
        Action { 
            text: qsTr("Diese Jahr") 
            onTriggered:dlsWidget.actionTrigger("gotoThisYearAction")
        }
        Action { 
            text: qsTr("Letztes Jahr") 
            onTriggered:dlsWidget.actionTrigger("gotoLastYearAction")
        }
    }
//FIXME some Action still missing
}

